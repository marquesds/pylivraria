CREATE TABLE livro(
	id int NOT NULL AUTO_INCREMENT, 
	titulo varchar(200) NOT NULL,
	genero varchar(200) NOT NULL,
	preco double NOT NULL,
	autor varchar(200),
	quantidade int NOT NULL,
	primary key(id));
